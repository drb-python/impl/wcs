from drb.exceptions.core import DrbException


class WcsRequestException(DrbException):
    pass
