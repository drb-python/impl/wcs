wheel>=0.37.0
twine>=3.4.2
pycodestyle>=2.7.0
coverage==5.1
coverage-badge==1.0.1
liccheck >= 0.7.2
build==0.10.0