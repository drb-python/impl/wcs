===================
Data Request Broker
===================
---------------------------------
Web Map Service driver for DRB
---------------------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-driver-wcs/month
    :target: https://pepy.tech/project/drb-driver-wcs
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-driver-wcs.svg
    :target: https://pypi.org/project/drb-driver-wcs/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-driver-wcs.svg
    :target: https://pypi.org/project/drb-driver-wcs/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-driver-wcs.svg
    :target: https://pypi.org/project/drb-driver-wcs/
    :alt: Python Version Support Badge

-------------------

This drb-driver-wcs module implements access to Web Coverage Service (WCS).



User Guide
==========

.. toctree::
   :maxdepth: 2

   user/install


.. toctree::

   dev/api



