.. _install:

Installation of wcs driver
====================================
Installing ``drb-driver-wcs`` with execute the following in a terminal:

.. code-block::

    pip install drb-driver-wcs
